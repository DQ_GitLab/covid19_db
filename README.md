**The impact of COVID-19**

At DATANOMIQ we are working on a report to analyze the economical impact of COVID-19 and for that we decided to bring data from different sources.

| Dataset | Definition |
| ------ | ------ |
| StockNames.csv | Provides name of the stock and which sector it belongs to |
| continents_dict.csv | Provides name of continent and the country |
| covid19.csv | Provides number of confirmed cases, deaths and recovered cases |
| covid_impact_education.csv | Provides the impact on education |
| crypto_data.csv | Provides crypto currencies data |
| stocks_data.csv | Provides stock prices |
| world_population.csv | Provides world population as of 2020 |


The data is provided in csv format and most datasets are updated once a day. 


**Acknowledgement**

The raw data pulled and arranged by Worldometers.info, OECD; Eurostat; World Bank; National Government Records, ACAPS, the Johns Hopkins University Center for Systems Science and Engineering (JHU CCSE) from the following resources:

1. World Health Organization (WHO): https://www.who.int/
1. DXY.cn. Pneumonia. 2020. http://3g.dxy.cn/newh5/view/pneumonia.
1. BNO News: https://bnonews.com/index.php/2020/02/the-latest-coronavirus-cases/
1. National Health Commission of the People’s Republic of China (NHC): http:://www.nhc.gov.cn/xcs/yqtb/list_gzbd.shtml
1. China CDC (CCDC): http:://weekly.chinacdc.cn/news/TrackingtheEpidemic.htm
1. Hong Kong Department of Health: https://www.chp.gov.hk/en/features/102465.html
1. Macau Government: https://www.ssm.gov.mo/portal/
1. Taiwan CDC: https://sites.google.com/cdc.gov.tw/2019ncov/taiwan?authuser=0
1. US CDC: https://www.cdc.gov/coronavirus/2019-ncov/index.html
1. Government of Canada: https://www.canada.ca/en/public-health/services/diseases/coronavirus.html
1. Australia Government Department of Health: https://www.health.gov.au/news/coronavirus-update-at-a-glance
1. European Centre for Disease Prevention and Control (ECDC): https://www.ecdc.europa.eu/en/geographical-distribution-2019-ncov-cases

