################################# 
# Load all important libraries 
#################################

install_and_load <- function(packages) {
  k <- packages[!(packages %in% installed.packages()[, "Package"])];
  if (length(k)) { install.packages(k, repos = 'https://cran.rstudio.com/'); }
  
  for (package_name in packages) { library(package_name, character.only = TRUE, quietly = TRUE); }
}

install_and_load(c("here","readr", "reshape2","dplyr","lubridate",
                   "quantmod","data.table","stringr","tidyr","XML","jsonlite","htmltab","httr","readxl"))




#path = here::here()
#setwd(path)

setwd("C:/Users/aakas/OneDrive - Herr Benjamin Aunkofer/Datanomiq_projects/COVID19_DB")
############################
# source all the scripts
##########################

source("src/generate_cases_csv.r")
source("src/generate_cases_csv_worldometer.r")
#source("src/get_crypto_data.r")
#source("src/get_stocks_names.r")
#source("src/get_world_population.r")
#source("src/get_economy_etfs.r")
#source("src/get_stocks_data.r")
source("src/get_economic_indicators_data.r")
#source("src/get_events_timeline.r")
