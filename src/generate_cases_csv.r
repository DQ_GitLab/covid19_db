
BASE_URL = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/'
CONFIRMED = 'time_series_covid19_confirmed_global.csv'
DEATH = 'time_series_covid19_deaths_global.csv'
RECOVERED = 'time_series_covid19_recovered_global.csv'



confirmed_cases <-read_csv(url(paste(BASE_URL,CONFIRMED,sep = '')))

death_cases <- read_csv(url(paste(BASE_URL,DEATH,sep = '')))

recovered_cases <-read_csv(url(paste(BASE_URL,RECOVERED,sep = '')))

###########################
# Data preprocessing
##########################

################################# 
# Change data from wide to long
#################################


melt_fun <- function(df) {
  
  df = reshape2::melt(df, id.vars=c("Province/State", "Country/Region","Lat","Long"))
  return(df)
  
}

# apply one function to all existing dataframes at once

#dflist <- Filter(is.data.frame, as.list(.GlobalEnv))
list2env(lapply(Filter(is.data.frame, as.list(.GlobalEnv)), melt_fun), envir = .GlobalEnv)


###############################
# Change column names
##############################

colnames(confirmed_cases) = c("State","Country","Lat","Long","Date","ConfirmedCases")

colnames(death_cases) = c("State","Country","Lat","Long","Date","Deaths")

colnames(recovered_cases) = c("State","Country","Lat","Long","Date","RecoveredCases")



##########################
confirmed_cases$Date = parse_date_time(confirmed_cases$Date,orders = c("ymd", "dmy", "mdy"))
death_cases$Date = parse_date_time(death_cases$Date,orders = c("ymd", "dmy", "mdy"))
recovered_cases$Date = parse_date_time(recovered_cases$Date,orders = c("ymd", "dmy", "mdy"))

x = c(max(confirmed_cases$Date),
      max(death_cases$Date),
      max(recovered_cases$Date))



confirmed_cases =  confirmed_cases %>% filter(Date <= min(x))
death_cases =  death_cases %>% filter(Date <= min(x))
recovered_cases =recovered_cases %>% filter(Date <= min(x))
##############################################

################################
## Merge dataframes
################################

covid19 <- merge(confirmed_cases,death_cases,by=c("State","Country","Date","Lat","Long"))
covid19 <- merge(covid19,recovered_cases,by=c("State","Country","Date","Lat","Long"))
covid19$Date<- lubridate::parse_date_time(covid19$Date, orders = c("ymd", "dmy", "mdy"))

covid19$updated_at <- max(covid19$Date)

covid19$script_runtime= Sys.time()

filename = paste0("datasets/", "covid19.csv")

write.csv(covid19,filename,row.names = F,fileEncoding = 'UTF-8')

rm(list=ls())





