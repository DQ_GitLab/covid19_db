

url = "https://www.worldometers.info/world-population/population-by-country/"


world_population <- htmltab(doc = url, which = "//th[text() = 'Country (or dependency)']/ancestor::table")


filename = paste0("datasets/", "world_population.csv")

write.csv(world_population,filename,row.names = F,fileEncoding = 'UTF-8')

rm(list=ls())
