
stocks = c('IWDA.L', 'EEM', 'WHCS.AS', 'IS0D.DE')
etf_desc = c('iShares Core MSCI World UCITS ETF',
             'iShares MSCI Emerging Markets ETF',
             'iShares MSCI World Health Care Sector',
             'iShares Oil & Gas Exploration & Production')



start <- as.Date("2020-01-01")
end <- as_date(Sys.Date())


for (i in 1:length(stocks)) {
  tryCatch({
    getSymbols(stocks[i], src = "yahoo", from = start, to = end)
  }, error=function(e){})
}



xtstodf <- function(xts_obj) {
  
  df =  data.frame(date=index(xts_obj), coredata(xts_obj))
  return(df)
  
}

list2env(lapply(Filter(is.xts, as.list(.GlobalEnv)), xtstodf), envir = .GlobalEnv)


clean_cols <- function(df) {
  
  colnames(df) = c("date","open","high",
                   "low","close","volume",
                   "adjusted")
  
  return(df)
  
}


list2env(lapply(Filter(is.data.frame, as.list(.GlobalEnv)), clean_cols), envir = .GlobalEnv)

dflist <- Filter(is.data.frame, as.list(.GlobalEnv))

dflist <- Map(cbind, dflist, stocks = names(dflist))


etf_economy<- rbindlist(dflist)

rm(list=ls()[ls() %in% stocks])
stocks_df = data.frame(stocks,etf_desc)

etf_economy <- etf_economy %>% left_join(stocks_df)


etf_economy= etf_economy %>% group_by(stocks) %>% mutate(rel_val = close-close[which.min(date)]) %>% ungroup()


filename = paste0("datasets/", "etf_economy.csv")

write.csv(etf_economy,filename,row.names = F,fileEncoding = 'UTF-8')

rm(list=ls())
