
#url = "https://www.acaps.org/sites/acaps/files/resources/files/20200409_acaps_-_covid-19_goverment_measures_dataset_v7.xlsx"

#url ="https://www.acaps.org/sites/acaps/files/resources/files/20200414_acaps_-_covid-19_goverment_measures_dataset_v7.xlsx"


url = "https://www.acaps.org/sites/acaps/files/resources/files/20200416_acaps_-_covid-19_goverment_measures_dataset_v8.xlsx"

GET(url, write_disk(tf <- tempfile(fileext = ".xlsx")))
events_timeline <- read_excel(tf, 2L)

events_timeline = events_timeline %>% select(COUNTRY,CATEGORY,MEASURE,DATE_IMPLEMENTED)
events_timeline$end = ""

filename = paste0("datasets/", "events_timline.csv")

write.csv(events_timeline,filename,row.names = F,fileEncoding = 'UTF-8')



events_timeline <- read_excel(tf, 2L)

events_timeline_detailed = events_timeline %>% select(COUNTRY,CATEGORY,MEASURE,DATE_IMPLEMENTED,COMMENTS)
events_timeline_detailed$end = ""


filename = paste0("datasets/", "events_timline_detailed.csv")

write.csv(events_timeline_detailed,filename,row.names = F,fileEncoding = 'UTF-8')



rm(list=ls())
